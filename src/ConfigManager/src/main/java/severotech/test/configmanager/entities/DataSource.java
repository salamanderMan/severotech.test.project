package severotech.test.configmanager.entities;
import com.google.gson.Gson;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DataSource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String address;
    private int port;
    private String dbName;
    private String login;
    private String password;
    private String roles;

    public DataSource() {
    }

    public DataSource(Long id, String name, String address, int port, String dbName, String login, String password, List<String> roles) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.port = port;
        this.dbName = dbName;
        this.login = login;
        this.password = password;
        convertRolesToString(roles);
    }

    public DataSource(String name, String address, int port, String dbName, String login, String password, List<String> roles) {
        this.name = name;
        this.address = address;
        this.port = port;
        this.dbName = dbName;
        this.login = login;
        this.password = password;
        convertRolesToString(roles);
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String connectionName) {
        this.name = connectionName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        Gson gson = new Gson();
        return gson.fromJson(roles,List.class);
    }

    public void setRoles(List<String> roles) {
        convertRolesToString(roles);
    }

    private void convertRolesToString(List<String> roles) {
        Gson gson = new Gson();
        this.roles = gson.toJson(roles);
    }
}
