package severotech.test.configmanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import severotech.test.configmanager.entities.DataSource;
import severotech.test.configmanager.repositories.DataSourceRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@EnableTransactionManagement
public class DataSourceService {
    @Autowired
    private DataSourceRepository dataSourceRepository;

    public DataSource create(String connectionName, String address, int port,String dbName, String login, String password, List<String> roles){
        return dataSourceRepository.save(new DataSource(connectionName, address, port, dbName, login,password,roles));
    }

    public DataSource update(long id, String connectionName, String address, int port, String dbName, String login, String password, List<String> roles){
        if(dataSourceRepository.existsById(id)){
            return dataSourceRepository.save(new DataSource(id, connectionName, address, port, dbName,login,password,roles));
        }
        else {
            return null;
        }
    }

    public DataSource read(long id, List<String> roles){
        DataSource dataSource = dataSourceRepository.findById(id).orElse(null);
        if(dataSource == null){
            return null;
        }
        if(dataSource.getRoles().stream().anyMatch(x->roles.contains(x))){
            return dataSource;
        }
        return null;
    }

    public void delete(long id){
        if(dataSourceRepository.existsById(id)){
            dataSourceRepository.deleteById(id);
        }
    }

    public List<DataSource> readAll(List<String> roles){
        List<DataSource> dataSources = dataSourceRepository.findAll().stream()
                .filter(dataSet->dataSet.getRoles().stream().anyMatch(x->roles.contains(x))).collect(Collectors.toList());
        return dataSources;
    }
}
