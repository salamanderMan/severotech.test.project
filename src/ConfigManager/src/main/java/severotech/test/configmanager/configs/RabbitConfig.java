package severotech.test.configmanager.configs;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static severotech.test.commonlibrary.constants.RabbitConstants.*;

@Configuration
public class RabbitConfig {

    //region DataSetCreate
    @Bean
    Queue queueCreateDataSet() {
        return new Queue(QUEUE_CREATE_DATA_SET, false);
    }
    @Bean
    Queue queueUpdateDataSet() {
        return new Queue(QUEUE_UPDATE_DATA_SET, false);
    }
    @Bean
    Queue queueReadDataSet() {
        return new Queue(QUEUE_READ_DATA_SET, false);
    }
    @Bean
    Queue queueDeleteDataSet() {
        return new Queue(QUEUE_DELETE_DATA_SET, false);
    }
    @Bean
    Queue queueReadAllDataSet() {
        return new Queue(QUEUE_READ_ALL_DATA_SET, false);
    }

    @Bean
    Binding bindingCreateDataSet(@Qualifier("queueCreateDataSet") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_CREATE_DATA_SET);
    }
    @Bean
    Binding bindingUpdateDataSet(@Qualifier("queueUpdateDataSet") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_UPDATE_DATA_SET);
    }
    @Bean
    Binding bindingReadDataSet(@Qualifier("queueReadDataSet") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_READ_DATA_SET);
    }
    @Bean
    Binding bindingDeleteDataSet(@Qualifier("queueDeleteDataSet") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_DELETE_DATA_SET);
    }
    @Bean
    Binding bindingReadAllDataSet(@Qualifier("queueReadAllDataSet") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_READ_ALL_DATA_SET);
    }
    //endregion
    //region DataSourceCreate
    @Bean
    Queue queueCreateDataSource() {
        return new Queue(QUEUE_CREATE_DATA_SOURCE, false);
    }
    @Bean
    Queue queueUpdateDataSource() {
        return new Queue(QUEUE_UPDATE_DATA_SOURCE, false);
    }
    @Bean
    Queue queueReadDataSource() {
        return new Queue(QUEUE_READ_DATA_SOURCE, false);
    }
    @Bean
    Queue queueDeleteDataSource() {
        return new Queue(QUEUE_DELETE_DATA_SOURCE, false);
    }
    @Bean
    Queue queueReadAllDataSource() {
        return new Queue(QUEUE_READ_ALL_DATA_SOURCE, false);
    }

    @Bean
    Binding bindingCreateDataSource(@Qualifier("queueCreateDataSource") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_CREATE_DATA_SOURCE);
    }
    @Bean
    Binding bindingUpdateDataSource(@Qualifier("queueUpdateDataSource") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_UPDATE_DATA_SOURCE);
    }
    @Bean
    Binding bindingReadDataSource(@Qualifier("queueReadDataSource") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_READ_DATA_SOURCE);
    }
    @Bean
    Binding bindingDeleteDataSource(@Qualifier("queueDeleteDataSource") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_DELETE_DATA_SOURCE);
    }
    @Bean
    Binding bindingReadAllDataSource(@Qualifier("queueReadAllDataSource") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_READ_ALL_DATA_SOURCE);
    }
    //endregion

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGE_CONFIG_MANAGER);
    }
}
