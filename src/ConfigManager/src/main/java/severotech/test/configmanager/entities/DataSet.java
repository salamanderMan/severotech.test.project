package severotech.test.configmanager.entities;

import com.google.gson.Gson;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
public class DataSet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String dataName;
    private long dataSourceId;
    private String queryTxt;
    private String roles;

    public DataSet() {
    }

    public DataSet(String dataName, long dataSourceId, String queryTxt, List<String> roles) {
        this.dataName = dataName;
        this.dataSourceId = dataSourceId;
        this.queryTxt = queryTxt;
        convertRolesToString(roles);
    }

    public DataSet(Long id, String dataName, long dataSourceId, String queryTxt, List<String> roles) {
        this.id = id;
        this.dataName = dataName;
        this.dataSourceId = dataSourceId;
        this.queryTxt = queryTxt;
        convertRolesToString(roles);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public long getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(long connectionName) {
        this.dataSourceId = connectionName;
    }

    public String getQueryTxt() {
        return queryTxt;
    }

    public void setQueryTxt(String queryTxt) {
        this.queryTxt = queryTxt;
    }

    public List<String> getRoles() {
        Gson gson = new Gson();
        return gson.fromJson(roles,List.class);
    }

    public void setRoles(List<String> roles) {
        convertRolesToString(roles);
    }

    private void convertRolesToString(List<String> roles) {
        Gson gson = new Gson();
        this.roles = gson.toJson(roles);
    }
}
