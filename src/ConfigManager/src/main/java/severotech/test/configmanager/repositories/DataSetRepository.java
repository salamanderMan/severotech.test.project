package severotech.test.configmanager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import severotech.test.configmanager.entities.DataSet;

@Repository
public interface DataSetRepository extends JpaRepository<DataSet, Long> {

}
