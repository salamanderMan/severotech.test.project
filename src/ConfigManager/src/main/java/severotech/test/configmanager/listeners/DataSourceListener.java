package severotech.test.configmanager.listeners;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import severotech.test.commonlibrary.dtos.DataSourceDTO;
import severotech.test.commonlibrary.dtos.RolesRequestDTO;
import severotech.test.configmanager.entities.DataSource;
import severotech.test.configmanager.services.DataSourceService;

import java.util.List;
import java.util.stream.Collectors;

import static severotech.test.commonlibrary.constants.RabbitConstants.*;

@Component
public class DataSourceListener {
    private Gson gson = new Gson();

    @Autowired
    private DataSourceService dataSourceService;

    @RabbitListener(queues = QUEUE_CREATE_DATA_SOURCE)
    public String create(Message message){
        String json = new String(message.getBody());
        DataSourceDTO dto =  gson.fromJson(json, DataSourceDTO.class);
        DataSource result = dataSourceService.create(dto.getName(),dto.getAddress(),dto.getPort(), dto.getDbName(),
                dto.getLogin(),dto.getPassword(),dto.getRoles());
        DataSourceDTO resultDTO = new DataSourceDTO(result.getId(), result.getName(),result.getAddress(),
                result.getPort(),result.getDbName(), result.getLogin(),result.getPassword(),result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_UPDATE_DATA_SOURCE)
    public String update(Message message){
        String json = new String(message.getBody());
        DataSourceDTO dto =  gson.fromJson(json, DataSourceDTO.class);
        DataSource result = dataSourceService.update(dto.getId(),dto.getName(),dto.getAddress(),dto.getPort(),dto.getDbName(), dto.getLogin(),
                dto.getPassword(),dto.getRoles());
        DataSourceDTO resultDTO = new DataSourceDTO(result.getId(), result.getName(), result.getAddress(), result.getPort(), result.getDbName(),
                result.getLogin(),result.getPassword(),result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_READ_DATA_SOURCE)
    public String read(Message message){
        String json = new String(message.getBody());
        RolesRequestDTO dto =  gson.fromJson(json, RolesRequestDTO.class);
        DataSource result = dataSourceService.read(dto.getId(), dto.getRoles());
        if(result ==null){
            return "No configs";
        }
        DataSourceDTO resultDTO = new DataSourceDTO(result.getId(), result.getName(),result.getAddress(),
                result.getPort(), result.getDbName(), result.getLogin(),result.getPassword(),result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_DELETE_DATA_SOURCE)
    public boolean delete(Message message){
        String json = new String(message.getBody());
        dataSourceService.delete(Integer.parseInt(json));
        return true;
    }
    @RabbitListener(queues = QUEUE_READ_ALL_DATA_SOURCE)
    public String readAll(Message message){
        String json = new String(message.getBody());
        RolesRequestDTO dto =  gson.fromJson(json, RolesRequestDTO.class);
        List<DataSource> result = dataSourceService.readAll(dto.getRoles());
        return gson.toJson(result.stream().map(x->new DataSourceDTO(x.getId(), x.getName(), x.getAddress(), x.getPort(), x.getDbName(),  x.getLogin(),
                x.getPassword(), x.getRoles())).collect(Collectors.toList()));
    }
}
