package severotech.test.configmanager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import severotech.test.configmanager.entities.DataSource;

@Repository
public interface DataSourceRepository extends JpaRepository<DataSource, Long> {

}
