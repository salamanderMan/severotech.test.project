package severotech.test.configmanager.listeners;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import severotech.test.commonlibrary.dtos.DataSetDTO;
import severotech.test.commonlibrary.dtos.RolesRequestDTO;
import severotech.test.configmanager.entities.DataSet;
import severotech.test.configmanager.services.DataSetService;

import java.util.List;
import java.util.stream.Collectors;

import static severotech.test.commonlibrary.constants.RabbitConstants.*;

@Component
public class DataSetListener {

    private Gson gson = new Gson();

    @Autowired
    private DataSetService dataSetService;

    @RabbitListener(queues = QUEUE_CREATE_DATA_SET)
    public String create(Message message){
        String json = new String(message.getBody());
        DataSetDTO dto =  gson.fromJson(json, DataSetDTO.class);
        DataSet result = dataSetService.create(dto.getName(),dto.getDataSourceId(),dto.getQueryTxt(),dto.getRoles());
        DataSetDTO resultDTO = new DataSetDTO(result.getId(), result.getDataName(), result.getDataSourceId(), result.getQueryTxt(), result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_UPDATE_DATA_SET)
    public String update(Message message){
        String json = new String(message.getBody());
        DataSetDTO dto =  gson.fromJson(json, DataSetDTO.class);
        DataSet result = dataSetService.update(dto.getId(),dto.getName(),dto.getDataSourceId(),dto.getQueryTxt(),dto.getRoles());
        DataSetDTO resultDTO = new DataSetDTO(result.getId(), result.getDataName(), result.getDataSourceId(), result.getQueryTxt(), result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_READ_DATA_SET)
    public String read(Message message){
        String json = new String(message.getBody());
        RolesRequestDTO dto =  gson.fromJson(json, RolesRequestDTO.class);
        DataSet result = dataSetService.read(dto.getId(),dto.getRoles());
        if(result ==null){
            return "No configs";
        }
        DataSetDTO resultDTO = new DataSetDTO(result.getId(), result.getDataName(), result.getDataSourceId(), result.getQueryTxt(), result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_DELETE_DATA_SET)
    public boolean delete(Message message){
        String json = new String(message.getBody());
        dataSetService.delete(Integer.parseInt(json));
        return true;
    }
    @RabbitListener(queues = QUEUE_READ_ALL_DATA_SET)
    public String readAll(Message message){
        String json = new String(message.getBody());
        RolesRequestDTO dto =  gson.fromJson(json, RolesRequestDTO.class);
        List<DataSet> result = dataSetService.readAll(dto.getRoles());
        return gson.toJson(result.stream().map(x->new DataSetDTO(x.getId(), x.getDataName(), x.getDataSourceId(),
                x.getQueryTxt(), x.getRoles())).collect(Collectors.toList()));
    }
}
