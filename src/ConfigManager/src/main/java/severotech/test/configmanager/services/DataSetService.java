package severotech.test.configmanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import severotech.test.configmanager.entities.DataSet;
import severotech.test.configmanager.repositories.DataSetRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@EnableTransactionManagement
public class DataSetService {
    @Autowired
    private DataSetRepository dataSetRepository;

    public DataSet create(String dataName, long connectionName, String queryTxt, List<String> roles){
        return dataSetRepository.save(new DataSet(dataName, connectionName, queryTxt, roles));
    }

    public DataSet update(long id, String dataName, long connectionName, String queryTxt, List<String> roles){
        if(dataSetRepository.existsById(id)){
            return dataSetRepository.save(new DataSet(id, dataName, connectionName, queryTxt, roles));
        }
        else {
            return null;
        }
    }

    public DataSet read(long id, List<String> roles){
        DataSet dataSet = dataSetRepository.findById(id).orElse(null);
        if(dataSet == null){
            return null;
        }
        if(dataSet.getRoles().stream().anyMatch(x->roles.contains(x))){
            return dataSet;
        }
        return null;
    }

    public void delete(long id){
        if(dataSetRepository.existsById(id)){
            dataSetRepository.deleteById(id);
        }
    }

    public List<DataSet> readAll(List<String> roles){
        List<DataSet> dataSets = dataSetRepository.findAll().stream()
                .filter(dataSet->dataSet.getRoles().stream().anyMatch(x->roles.contains(x))).collect(Collectors.toList());
        return dataSets;
    }
}
