package severotech.test.datareader.services;

import com.google.gson.Gson;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import severotech.test.commonlibrary.dtos.DataSetDTO;
import severotech.test.commonlibrary.dtos.DataSourceDTO;
import severotech.test.commonlibrary.dtos.RolesRequestDTO;
import severotech.test.datareader.storage.DataSourceConnector;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static severotech.test.commonlibrary.constants.RabbitConstants.*;

@Service
public class DataReadService {

    private Gson gson = new Gson();
    private RabbitTemplate rabbitTemplate;
    private DataSourceConnector dataSourceConnector;

    public DataReadService(@Autowired DataSourceConnector dataSourceConnector, @Autowired RabbitTemplate rabbitTemplate) {
        this.dataSourceConnector = dataSourceConnector;
        this.rabbitTemplate = rabbitTemplate;
    }

    public List<Map<String, Object>> executeQuery(JdbcTemplate tmp, String queryTxt){
        return tmp.queryForList(queryTxt);
    }
    public List<Map<String, Object>> executeQuery(String username, String password, String serverName, int port, String databaseName, String queryTxt){
        JdbcTemplate jdbcTemplate = connectToDataSource(username, password, serverName, port, databaseName);
        return executeQuery(jdbcTemplate,queryTxt);
    }

    public List<Map<String, Object>> getData(long dataSetId, List<String> roles){
        DataSetDTO dataSetDTO = readDataSet(dataSetId,roles);
        if(dataSetDTO == null){
            return new ArrayList<>();
        }
        DataSourceDTO dataSourceDTO = readDataSource(dataSetDTO.getDataSourceId(), roles);
        if(dataSourceDTO == null){
            return new ArrayList<>();
        }
        List<Map<String, Object>> maps = executeQuery(dataSourceDTO.getLogin(), dataSourceDTO.getPassword(), dataSourceDTO.getAddress(),
                dataSourceDTO.getPort(), dataSourceDTO.getDbName(), dataSetDTO.getQueryTxt());
        return maps;
    }

    public JdbcTemplate connectToDataSource(String username, String password, String serverName, int port, String databaseName) {
        return dataSourceConnector.connectToDataSource(username, password, serverName, port, databaseName);
    }

    public DataSourceDTO readDataSource(@RequestParam long id, List<String> roles){
        String receive = (String)rabbitTemplate.convertSendAndReceive(QUEUE_READ_DATA_SOURCE,
                gson.toJson(new RolesRequestDTO(id, roles)));
        if(!receive.equals("No configs")){
            return gson.fromJson(receive,DataSourceDTO.class);
        }
        return null;
    }
    public DataSetDTO readDataSet(@RequestParam long id, List<String> roles){
        String receive = (String)rabbitTemplate.convertSendAndReceive(QUEUE_READ_DATA_SET,
                gson.toJson(new RolesRequestDTO(id, roles)));
        if(!receive.equals("No configs")){
            return gson.fromJson(receive,DataSetDTO.class);
        }
        return null;
    }
}
