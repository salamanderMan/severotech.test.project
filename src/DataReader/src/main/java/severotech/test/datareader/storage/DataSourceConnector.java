package severotech.test.datareader.storage;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class DataSourceConnector {
    public JdbcTemplate connectToDataSource(String username, String password, String serverName, int port, String databaseName){
        DataSource dataSource = DataSourceBuilder
                .create()
                .username(username)
                .password(password)
                .url(String.format("jdbc:postgresql://%s:%d/%s",serverName,port,databaseName))
                .driverClassName("org.postgresql.Driver")
                .build();
        return new JdbcTemplate(dataSource);
    }
}
