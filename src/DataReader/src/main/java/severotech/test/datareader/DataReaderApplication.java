package severotech.test.datareader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class})
public class DataReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataReaderApplication.class, args);
    }

}
