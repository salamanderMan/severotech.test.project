package severotech.test.datareader.listeners;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import severotech.test.commonlibrary.dtos.ReadDataSourceResponse;
import severotech.test.commonlibrary.dtos.RolesRequestDTO;
import severotech.test.commonlibrary.dtos.UserDTO;
import severotech.test.datareader.services.DataReadService;

import static severotech.test.commonlibrary.constants.RabbitConstants.QUEUE_READ_DATA;

@Component
public class DataReadListener {

    @Autowired
    DataReadService dataReadService;

    private Gson gson = new Gson();

    @RabbitListener(queues = QUEUE_READ_DATA)
    public String dataRead(Message message){
        String json = new String(message.getBody());
        RolesRequestDTO dto =  gson.fromJson(json, RolesRequestDTO.class);
        ReadDataSourceResponse result = new ReadDataSourceResponse(dataReadService.getData(dto.getId(),dto.getRoles()));
        return gson.toJson(result);
    }
}
