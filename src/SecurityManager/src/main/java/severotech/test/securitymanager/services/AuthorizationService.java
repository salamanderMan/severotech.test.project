package severotech.test.securitymanager.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.AccessException;
import org.springframework.stereotype.Service;
import severotech.test.commonlibrary.dtos.TokenDTO;
import severotech.test.securitymanager.entities.User;
import severotech.test.securitymanager.entities.UserRole;
import severotech.test.securitymanager.helpers.HashHelper;
import severotech.test.securitymanager.repositories.UserRepository;
import severotech.test.securitymanager.repositories.UserRoleRepository;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AuthorizationService {

    //;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private HashHelper hashHelper;

    public TokenDTO login(String login, String password){
        User user = userRepository.findByLogin(login);
        if(user!=null && hashHelper.verifyHash(password, user.getPassword())){
            Date expiresDate = getExpiresDate();
            Algorithm algorithm = Algorithm.HMAC256("DEE3B166F14C712AD16F1DD6754A6");
            String token = JWT.create()
                    .withIssuer("securityManager")
                    .withClaim("roles", user.getRoles())
                    .withExpiresAt(expiresDate)
                    .sign(algorithm);
            JWT.decode(token).getSignature();
            return new TokenDTO(token, expiresDate, null);
        }
        return new TokenDTO(null,null,"Error: Can not authorize. Login or password incorrect");
    }

    public List<String> validate(TokenDTO token) throws AccessException {
        DecodedJWT decode = JWT.decode(token.getToken());
        Algorithm algorithm = Algorithm.HMAC256("DEE3B166F14C712AD16F1DD6754A6");
        algorithm.verify(decode);
        if(decode.getExpiresAt().getTime() > System.currentTimeMillis()){
            throw new TokenExpiredException("Expired at " + decode.getExpiresAt());
        };
        Gson gson = new Gson();
        List<String> roles = gson.fromJson(decode.getClaim("roles").toString(), List.class);
        checkAccess(token, roles);
        return roles;
    }

    private void checkAccess(TokenDTO token, List<String> roles) throws AccessException {
        List<UserRole> rolesByNameIn = userRoleRepository.findByNameIn(roles);
        for(UserRole role : rolesByNameIn){
            switch (token.getTargetService()){
                case DATA_SET:
                    if(role.getDataSetOperations().contains(token.getOperationType().toString())== false){
                        throw new AccessException("Access denied");
                    }
                    break;
                case DATA_SOURCE:
                    if(role.getDataSourceOperations().contains(token.getOperationType().toString())== false){
                        throw new AccessException("Access denied");
                    }
                    break;
                case USER:
                    if(role.getUserOperations().contains(token.getOperationType().toString()) == false){
                        throw new AccessException("Access denied");
                    }
                    break;
            }
        }
    }

    private Date getExpiresDate() {
        Instant nowUtc = Instant.now();
        Date currentDate = Date.from(nowUtc);
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.MINUTE, 15);
        c.getTime();
        return currentDate;
    }

//    Instant nowUtc = Instant.now();
//    Date currentDate = Date.from(nowUtc);
//    Calendar c = Calendar.getInstance();
//c.setTime(currentDate);
//c.add(Calendar.SECOND, 15);
//c.getTime();
//    String token1 =JWT.create()
//            .withIssuer("securityManager")
//            .withClaim("roles", user.getRoles())
//            .withExpiresAt(c.getTime())
//            .sign(algorithm);
//algorithm.verify(JWT.decode(token1));
//if(JWT.decode(token1).getExpiresAt().getTime() < System.currentTimeMillis()){
//        int a = 5;
//    };

}
