package severotech.test.securitymanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import severotech.test.securitymanager.entities.User;
import severotech.test.securitymanager.helpers.HashHelper;
import severotech.test.securitymanager.repositories.UserRepository;

import java.util.List;
import java.util.stream.Collectors;


@Service
@EnableTransactionManagement
public class UserServices {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HashHelper hashHelper;

    public User create(String login, String password, List<String> roles){
        return userRepository.save(new User(login, hashHelper.hash(password), roles));
    }

    public User update(long id, String login, String password, List<String> roles){
        if(userRepository.existsById(id)){
            return userRepository.save(new User(id, login, hashHelper.hash(password), roles));
        }
        else {
            return null;
        }
    }

    public User read(long id, List<String> roles){
        User user = userRepository.findById(id).orElse(null);
        if(user.getRoles().stream().anyMatch(x->roles.contains(x))){
            return user;
        }
        return null;
    }

    public void delete(long id){
        if(userRepository.existsById(id)){
            userRepository.deleteById(id);
        }
    }

    public List<User> readAll(List<String> roles){
        List<User> users = userRepository.findAll().stream()
                .filter(dataSet->dataSet.getRoles().stream().anyMatch(x->roles.contains(x))).collect(Collectors.toList());
        return users;
    }


}
