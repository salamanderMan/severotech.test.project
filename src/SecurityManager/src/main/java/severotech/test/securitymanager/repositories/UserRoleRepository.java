package severotech.test.securitymanager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import severotech.test.securitymanager.entities.UserRole;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRole, String> {
    List<UserRole> findByNameIn(List<String> names);
}
