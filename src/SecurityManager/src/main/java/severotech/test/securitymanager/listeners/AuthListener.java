package severotech.test.securitymanager.listeners;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import severotech.test.commonlibrary.dtos.AuthDTO;
import severotech.test.commonlibrary.dtos.TokenDTO;
import severotech.test.commonlibrary.dtos.ValidationResultDTO;
import severotech.test.securitymanager.services.AuthorizationService;

import static severotech.test.commonlibrary.constants.RabbitConstants.QUEUE_TOKEN_CREATE;
import static severotech.test.commonlibrary.constants.RabbitConstants.QUEUE_TOKEN_VALIDATE;

@Component
public class AuthListener {
    private Gson gson = new Gson();

    @Autowired
    AuthorizationService authorizationService;

    @RabbitListener(queues = QUEUE_TOKEN_CREATE)
    public String createToken(Message message){
        String json = new String(message.getBody());
        AuthDTO dto =  gson.fromJson(json, AuthDTO.class);
        TokenDTO result = authorizationService.login(dto.getLogin(),dto.getPassword());
        return gson.toJson(result);
    }

    @RabbitListener(queues = QUEUE_TOKEN_VALIDATE)
    public String validate(Message message){
        String json = new String(message.getBody());
        TokenDTO dto =  gson.fromJson(json, TokenDTO.class);
        ValidationResultDTO result;
        try {
            result = new ValidationResultDTO(authorizationService.validate(dto));
        }
        catch (Exception ex){
            result = new ValidationResultDTO(ex.getMessage());
        }
        return gson.toJson(result);
    }
}
