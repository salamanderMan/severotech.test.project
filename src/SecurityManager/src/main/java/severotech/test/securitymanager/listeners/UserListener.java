package severotech.test.securitymanager.listeners;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import severotech.test.commonlibrary.dtos.RolesRequestDTO;
import severotech.test.commonlibrary.dtos.UserDTO;
import severotech.test.securitymanager.entities.User;
import severotech.test.securitymanager.services.UserServices;

import java.util.List;
import java.util.stream.Collectors;

import static severotech.test.commonlibrary.constants.RabbitConstants.*;

@Component
public class UserListener {
    private Gson gson = new Gson();

    @Autowired
    private UserServices userServices;

    @RabbitListener(queues = QUEUE_CREATE_USER)
    public String create(Message message){
        String json = new String(message.getBody());
        UserDTO dto =  gson.fromJson(json, UserDTO.class);
        User result = userServices.create(dto.getLogin(),dto.getPassword(),dto.getRoles());
        UserDTO resultDTO = new UserDTO(result.getId(),result.getLogin(),result.getPassword(),result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_UPDATE_USER)
    public String update(Message message){
        String json = new String(message.getBody());
        UserDTO dto =  gson.fromJson(json, UserDTO.class);
        User result = userServices.update(dto.getId(),dto.getLogin(),dto.getPassword(),dto.getRoles());
        UserDTO resultDTO = new UserDTO(result.getId(),result.getLogin(),result.getPassword(),result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_READ_USER)
    public String read(Message message){
        String json = new String(message.getBody());
        RolesRequestDTO dto =  gson.fromJson(json, RolesRequestDTO.class);
        User result = userServices.read(dto.getId(), dto.getRoles());
        UserDTO resultDTO = new UserDTO(result.getId(),result.getLogin(),result.getPassword(),result.getRoles());
        return gson.toJson(resultDTO);
    }
    @RabbitListener(queues = QUEUE_DELETE_USER)
    public boolean delete(Message message){
        String json = new String(message.getBody());
        userServices.delete(Integer.parseInt(json));
        return true;
    }
    @RabbitListener(queues = QUEUE_READ_ALL_USER)
    public String readAll(Message message){
        String json = new String(message.getBody());
        RolesRequestDTO dto =  gson.fromJson(json, RolesRequestDTO.class);
        List<User> result = userServices.readAll(dto.getRoles());
        return gson.toJson(result.stream().map(x->new UserDTO(x.getId(), x.getLogin(), x.getPassword(), x.getRoles())).collect(Collectors.toList()));
    }
}
