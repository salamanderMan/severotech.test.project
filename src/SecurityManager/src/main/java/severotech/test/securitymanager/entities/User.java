package severotech.test.securitymanager.entities;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.List;

@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true)
    private String login;
    private String password;
    private String roles;

    public User(long id, String login, String password, List<String> roles) {
        this.id = id;
        this.login = login;
        this.password = password;
        convertRolesToString(roles);
    }

    public User(String login, String password, List<String> roles) {
        this.login = login;
        this.password = password;
        convertRolesToString(roles);
    }

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        Gson gson = new Gson();
        return gson.fromJson(roles,List.class);
    }

    public void setRoles(List<String> roles) {
        convertRolesToString(roles);
    }

    private void convertRolesToString(List<String> roles) {
        Gson gson = new Gson();
        this.roles = gson.toJson(roles);
    }
}
