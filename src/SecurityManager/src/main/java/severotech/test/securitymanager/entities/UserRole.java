package severotech.test.securitymanager.entities;

import javax.persistence.*;

@Entity(name = "user_roles")
public class UserRole {
    @Id
    @Column(name = "role_name")
    private String name;
    @Column(name = "operations_list_data_set")
    private String dataSetOperations;
    @Column(name = "operations_list_data_source")
    private String dataSourceOperations;
    @Column(name = "operations_list_user")
    private String userOperations;

    public UserRole(String name, String dataSetOperations, String dataSourceOperations, String userOperations) {
        this.name = name;
        this.dataSetOperations = dataSetOperations;
        this.dataSourceOperations = dataSourceOperations;
        this.userOperations = userOperations;
    }

    public UserRole() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataSetOperations() {
        return dataSetOperations;
    }

    public void setDataSetOperations(String dataSetOperations) {
        this.dataSetOperations = dataSetOperations;
    }

    public String getDataSourceOperations() {
        return dataSourceOperations;
    }

    public void setDataSourceOperations(String dataSourceOperations) {
        this.dataSourceOperations = dataSourceOperations;
    }

    public String getUserOperations() {
        return userOperations;
    }

    public void setUserOperations(String userOperations) {
        this.userOperations = userOperations;
    }
}
