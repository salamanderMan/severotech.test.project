package severotech.test.securitymanager.configs;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static severotech.test.commonlibrary.constants.RabbitConstants.*;

@Configuration
public class RabbitConfig {

    //region User
    @Bean
    Queue queueCreateUser() {
        return new Queue(QUEUE_CREATE_USER, false);
    }
    @Bean
    Queue queueUpdateUser() {
        return new Queue(QUEUE_UPDATE_USER, false);
    }
    @Bean
    Queue queueReadUser() {
        return new Queue(QUEUE_READ_USER, false);
    }
    @Bean
    Queue queueDeleteUser() {
        return new Queue(QUEUE_DELETE_USER, false);
    }
    @Bean
    Queue queueReadAllUser() {
        return new Queue(QUEUE_READ_ALL_USER, false);
    }

    @Bean
    Binding bindingCreateDataSet(@Qualifier("queueCreateUser") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_CREATE_USER);
    }
    @Bean
    Binding bindingUpdateDataSet(@Qualifier("queueUpdateUser") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_UPDATE_USER);
    }
    @Bean
    Binding bindingReadDataSet(@Qualifier("queueReadUser") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_READ_USER);
    }
    @Bean
    Binding bindingDeleteDataSet(@Qualifier("queueDeleteUser") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_DELETE_USER);
    }
    @Bean
    Binding bindingReadAllDataSet(@Qualifier("queueReadAllUser") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_READ_ALL_USER);
    }
//endregion

    //region Token
    @Bean
    Queue queueCreateToken() {
        return new Queue(QUEUE_TOKEN_CREATE, false);
    }

    @Bean
    Queue queueValidateToken() {
        return new Queue(QUEUE_TOKEN_VALIDATE, false);
    }

    @Bean
    Binding bindingCreateToken(@Qualifier("queueCreateToken") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_TOKEN_CREATE);
    }
    @Bean
    Binding bindingValidateToken(@Qualifier("queueValidateToken") Queue queue, @Qualifier("exchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_TOKEN_VALIDATE);
    }

    //endregion

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGE_SECURITY_MANAGER);
    }
}
