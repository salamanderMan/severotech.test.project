DELETE FROM public.user_roles;
INSERT INTO  public.user_roles(
    role_name, operations_list_data_set, operations_list_data_source, operations_list_user) VALUES ('admin','["CREATE","READ","UPDATE","DELETE"]','["CREATE","READ","UPDATE","DELETE"]','["CREATE","READ","UPDATE","DELETE"]');
INSERT INTO  public.user_roles(
    role_name, operations_list_data_set, operations_list_data_source, operations_list_user) VALUES ('guest','["READ"]','["READ"]','["READ"]');
INSERT INTO  public.user_roles(
    role_name, operations_list_data_set, operations_list_data_source, operations_list_user) VALUES ('operator','["CREATE","READ","UPDATE","DELETE"]','["CREATE","READ","UPDATE","DELETE"]','["READ"]');
INSERT INTO  public.user_roles(
    role_name, operations_list_data_set, operations_list_data_source, operations_list_user) VALUES ('viewer','["READ"]','["READ"]','["READ"]');



