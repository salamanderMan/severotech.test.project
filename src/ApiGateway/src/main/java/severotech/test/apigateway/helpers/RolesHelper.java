package severotech.test.apigateway.helpers;

import com.google.gson.Gson;
import org.apache.logging.log4j.util.Strings;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import severotech.test.commonlibrary.dtos.TokenDTO;
import severotech.test.commonlibrary.dtos.ValidationResultDTO;
import severotech.test.commonlibrary.enums.OperationType;
import severotech.test.commonlibrary.enums.TargetService;

import java.util.List;

import static severotech.test.commonlibrary.constants.RabbitConstants.QUEUE_TOKEN_VALIDATE;

@Component
public class RolesHelper {

    @Autowired
    RabbitTemplate template;

    Gson gson = new Gson();

    public List<String> checkRoles(String token, TargetService targetService, OperationType operationType) {
        if(Strings.isBlank(token)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No token");
        }
        TokenDTO tokenDTO = new TokenDTO(token, targetService, operationType);
        String receive = (String)template.convertSendAndReceive(QUEUE_TOKEN_VALIDATE,
                gson.toJson(tokenDTO));
        ValidationResultDTO validationResult = gson.fromJson(receive, ValidationResultDTO.class);
        if(Strings.isBlank(validationResult.getExMessage())){
            return validationResult.getRoles();
        }
        throw new ResponseStatusException(HttpStatus.FORBIDDEN, validationResult.getExMessage());
    }
}
