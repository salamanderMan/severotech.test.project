package severotech.test.apigateway.controllers;

import com.google.gson.Gson;
import io.swagger.annotations.Api;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import severotech.test.apigateway.helpers.RolesHelper;
import severotech.test.commonlibrary.dtos.RolesRequestDTO;
import severotech.test.commonlibrary.enums.OperationType;
import severotech.test.commonlibrary.enums.TargetService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

import static severotech.test.commonlibrary.constants.RabbitConstants.QUEUE_READ_DATA;
import static severotech.test.commonlibrary.constants.RabbitConstants.QUEUE_READ_DATA_SET;

@RestController
@RequestMapping("/dataread")
@Api(tags = "DataRead")
public class DataReadController {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private RolesHelper rolesHelper;

    private Gson gson = new Gson();
    @GetMapping
    public ResponseEntity<String> readData(@RequestParam long id, @RequestHeader(value = "Authorization") @ApiIgnore String token){
        List<String> roles = rolesHelper.checkRoles(token, TargetService.DATA_SET, OperationType.READ);
        Gson gson = new Gson();
        String receive = (String)template.convertSendAndReceive( QUEUE_READ_DATA,
                gson.toJson(new RolesRequestDTO(id, roles)));
        return new ResponseEntity<>(receive, HttpStatus.OK);
    }
}
