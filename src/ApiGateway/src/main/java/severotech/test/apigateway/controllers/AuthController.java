package severotech.test.apigateway.controllers;

import com.google.gson.Gson;
import io.swagger.annotations.Api;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import severotech.test.commonlibrary.dtos.AuthDTO;

import static severotech.test.commonlibrary.constants.RabbitConstants.QUEUE_TOKEN_CREATE;

@RestController
@RequestMapping("/auth")
@Api(tags = "Auth")
public class AuthController {
    @Autowired
    private RabbitTemplate template;
    Gson gson = new Gson();

    @PostMapping
    public ResponseEntity<String> auth(@RequestBody AuthDTO authDTO){
        String receive = (String)template.convertSendAndReceive(QUEUE_TOKEN_CREATE,
                gson.toJson(authDTO));
        return new ResponseEntity<>(receive, HttpStatus.OK);
    }
}
