package severotech.test.apigateway.controllers;

import com.google.gson.Gson;
import io.swagger.annotations.Api;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import severotech.test.apigateway.helpers.RolesHelper;
import severotech.test.commonlibrary.dtos.RolesRequestDTO;
import severotech.test.commonlibrary.dtos.UserDTO;
import severotech.test.commonlibrary.enums.OperationType;
import severotech.test.commonlibrary.enums.TargetService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Objects;

import static severotech.test.commonlibrary.constants.RabbitConstants.*;

@RestController
@RequestMapping("/user")
@Api(tags = "User")
public class UserController {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private RolesHelper rolesHelper;

    Gson gson = new Gson();

    @PostMapping
    public ResponseEntity<String> create(@RequestBody UserDTO dto, @RequestHeader(value = "Authorization") @ApiIgnore String token){
        rolesHelper.checkRoles(token, TargetService.USER, OperationType.CREATE);
        dto.setId(0l);
        String receive = (String)template.convertSendAndReceive(QUEUE_CREATE_USER,
                gson.toJson(dto));
        return new ResponseEntity<>(receive, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestBody UserDTO dto, @RequestHeader(value = "Authorization") @ApiIgnore String token){
        rolesHelper.checkRoles(token, TargetService.USER, OperationType.UPDATE);
        String receive = (String)template.convertSendAndReceive(QUEUE_UPDATE_USER,
                gson.toJson(dto));
        return new ResponseEntity<>(receive, HttpStatus.OK);
    }
    @GetMapping
    public ResponseEntity<String> read(@RequestParam long id, @RequestHeader(value = "Authorization") @ApiIgnore String token){
        List<String> roles = rolesHelper.checkRoles(token, TargetService.USER, OperationType.READ);
        String receive = (String)template.convertSendAndReceive(QUEUE_READ_USER,
                gson.toJson(new RolesRequestDTO(id,roles)));
        return new ResponseEntity<>(receive, HttpStatus.OK);
    }
    @DeleteMapping
    public ResponseEntity<Boolean> delete(@RequestParam long id, @RequestHeader(value = "Authorization") @ApiIgnore String token){
        rolesHelper.checkRoles(token, TargetService.USER, OperationType.DELETE);
        Boolean receive = (Boolean) template.convertSendAndReceive(QUEUE_DELETE_USER,
                Objects.toString(id));
        return new ResponseEntity<>(receive, HttpStatus.OK);
    }
    @GetMapping("/readall")
    public ResponseEntity<String> readAll(@RequestHeader(value = "Authorization") @ApiIgnore String token){
        List<String> roles = rolesHelper.checkRoles(token, TargetService.USER, OperationType.READ);
        String receive = (String)template.convertSendAndReceive(QUEUE_READ_ALL_USER, gson.toJson(new RolesRequestDTO(0,roles)));
        return new ResponseEntity<>(receive, HttpStatus.OK);
    }
}
