package severotech.test.apigateway.controllers;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@Api(tags = "Hello")
public class HelloController {
    @GetMapping("/")

    public String hello(){
        return "hello";
    }
}
