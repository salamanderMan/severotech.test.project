package severotech.test.commonlibrary.dtos;

import java.util.List;

public class ValidationResultDTO {
    List<String> roles;
    String exMessage;

    public ValidationResultDTO(List<String> roles) {
        this.roles = roles;
    }

    public ValidationResultDTO(String exMessage) {
        this.exMessage = exMessage;
    }

    public ValidationResultDTO() {
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getExMessage() {
        return exMessage;
    }

    public void setExMessage(String exMessage) {
        this.exMessage = exMessage;
    }
}
