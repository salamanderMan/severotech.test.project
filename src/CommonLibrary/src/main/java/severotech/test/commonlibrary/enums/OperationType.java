package severotech.test.commonlibrary.enums;

public enum OperationType {
    NONE,
    CREATE,
    READ,
    UPDATE,
    DELETE;
}
