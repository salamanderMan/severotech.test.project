package severotech.test.commonlibrary.dtos;

import java.util.List;

public class DataSourceDTO {
    private long id;
    private String name;
    private String address;
    private int port;
    private String dbName;
    private String login;
    private String password;
    private List<String> roles;

    public DataSourceDTO(long id, String name, String address, int port, String dbName, String login, String password, List<String> roles) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.port = port;
        this.dbName = dbName;
        this.login = login;
        this.password = password;
        this.roles = roles;
    }

    public DataSourceDTO(String name, String address, int port, String dbName, String login, String password, List<String> roles) {
        this.name = name;
        this.address = address;
        this.port = port;
        this.dbName = dbName;
        this.login = login;
        this.password = password;
        this.roles = roles;
    }

    public DataSourceDTO() {
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

}
