package severotech.test.commonlibrary.enums;

public enum TargetService {
    DATA_SET,
    DATA_SOURCE,
    USER;
}
