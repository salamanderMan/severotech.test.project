package severotech.test.commonlibrary.dtos;

import java.util.List;

public class RolesRequestDTO {
    private  long id;
    private List<String> roles;

    public RolesRequestDTO(long id, List<String> roles) {
        this.id = id;
        this.roles = roles;
    }

    public RolesRequestDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
