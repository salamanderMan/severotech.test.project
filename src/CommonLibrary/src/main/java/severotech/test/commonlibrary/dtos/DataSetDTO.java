package severotech.test.commonlibrary.dtos;

import java.util.List;

public class DataSetDTO {
    private long id;
    private String name;
    private long dataSourceId;
    private String queryTxt;
    private List<String> roles;

    public DataSetDTO(String name, long dataSourceId, String queryTxt, List<String> roles) {
        this.name = name;
        this.dataSourceId = dataSourceId;
        this.queryTxt = queryTxt;
        this.roles = roles;
    }

    public DataSetDTO(Long id, String name, long dataSourceId, String queryTxt, List<String> roles) {
        this.id = id;
        this.name = name;
        this.dataSourceId = dataSourceId;
        this.queryTxt = queryTxt;
        this.roles = roles;
    }

    public DataSetDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(long dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public String getQueryTxt() {
        return queryTxt;
    }

    public void setQueryTxt(String queryTxt) {
        this.queryTxt = queryTxt;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
