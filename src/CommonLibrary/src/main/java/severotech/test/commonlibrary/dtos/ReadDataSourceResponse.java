package severotech.test.commonlibrary.dtos;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ReadDataSourceResponse {
    List<Map<String, Object>> result;

    public List<Map<String, Object>> getResult() {
        return result;
    }

    public void setResult(List<Map<String, Object>> result) {
        this.result = result;
    }

    public ReadDataSourceResponse(List<Map<String, Object>> result) {
        this.result = result;
    }

    public ReadDataSourceResponse() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReadDataSourceResponse that = (ReadDataSourceResponse) o;
        return Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result);
    }

    @Override
    public String toString() {
        return "ReadDataSourceResponse{" +
                "result=" + result +
                '}';
    }
}
