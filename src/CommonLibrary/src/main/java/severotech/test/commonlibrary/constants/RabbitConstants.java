package severotech.test.commonlibrary.constants;

public class RabbitConstants {

    public static final String ROUTING_KEY_CREATE_DATA_SET = "DataSetCreate";
    public static final String ROUTING_KEY_UPDATE_DATA_SET = "DataSetUpdate";
    public static final String ROUTING_KEY_READ_DATA_SET = "DataSetRead";
    public static final String ROUTING_KEY_DELETE_DATA_SET = "DataSetDelete";
    public static final String ROUTING_KEY_READ_ALL_DATA_SET = "DataSetReadAll";

    public static final String QUEUE_CREATE_DATA_SET = "DataSetCreateQueue";
    public static final String QUEUE_UPDATE_DATA_SET = "DataSetUpdateQueue";
    public static final String QUEUE_READ_DATA_SET = "DataSetReadQueue";
    public static final String QUEUE_DELETE_DATA_SET = "DataSetDeleteQueue";
    public static final String QUEUE_READ_ALL_DATA_SET = "DataSetReadAllQueue";

    public static final String ROUTING_KEY_CREATE_DATA_SOURCE = "DataSourceCreate";
    public static final String ROUTING_KEY_UPDATE_DATA_SOURCE = "DataSourceUpdate";
    public static final String ROUTING_KEY_READ_DATA_SOURCE= "DataSourceRead";
    public static final String ROUTING_KEY_DELETE_DATA_SOURCE = "DataSourceDelete";
    public static final String ROUTING_KEY_READ_ALL_DATA_SOURCE = "DataSourceReadAll";

    public static final String QUEUE_CREATE_DATA_SOURCE= "DataSourceCreateQueue";
    public static final String QUEUE_UPDATE_DATA_SOURCE = "DataSourceUpdateQueue";
    public static final String QUEUE_READ_DATA_SOURCE = "DataSourceReadQueue";
    public static final String QUEUE_DELETE_DATA_SOURCE = "DataSourceDeleteQueue";
    public static final String QUEUE_READ_ALL_DATA_SOURCE = "DataSourceReadAllQueue";

    public static final String ROUTING_KEY_CREATE_USER = "UserCreate";
    public static final String ROUTING_KEY_UPDATE_USER = "UserUpdate";
    public static final String ROUTING_KEY_READ_USER= "UserRead";
    public static final String ROUTING_KEY_DELETE_USER = "UserDelete";
    public static final String ROUTING_KEY_READ_ALL_USER = "UserReadAll";

    public static final String QUEUE_CREATE_USER = "UserCreateQueue";
    public static final String QUEUE_UPDATE_USER = "UserUpdateQueue";
    public static final String QUEUE_READ_USER = "UserReadQueue";
    public static final String QUEUE_DELETE_USER = "UserDeleteQueue";
    public static final String QUEUE_READ_ALL_USER = "UserReadAllQueue";

    public static final String ROUTING_KEY_TOKEN_CREATE = "TokenCreate";
    public static final String ROUTING_KEY_TOKEN_VALIDATE = "TokenValidate";

    public static final String QUEUE_TOKEN_CREATE = "TokenCreateQueue";
    public static final String QUEUE_TOKEN_VALIDATE = "TokenValidateQueue";

    public static final String QUEUE_READ_DATA = "DataReadQueue";
    public static final String ROUTING_KEY_READ_DATA = "DataRead";

    public static final String EXCHANGE_CONFIG_MANAGER = "ConfigManagerExchange";
    public static final String EXCHANGE_SECURITY_MANAGER = "SecurityManagerExchange";
    public static final String EXCHANGE_DATA_READ = "DataReadExchange";


}
