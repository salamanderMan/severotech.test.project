package severotech.test.commonlibrary.dtos;

import severotech.test.commonlibrary.enums.OperationType;
import severotech.test.commonlibrary.enums.TargetService;

import java.util.Date;

public class TokenDTO {
    private String token;
    private Date expiresDate;
    private String error;
    private TargetService targetService;
    private OperationType operationType;

    public TokenDTO(String token, Date expiresDate, String error, TargetService targetService, OperationType operationType) {
        this.token = token;
        this.expiresDate = expiresDate;
        this.error = error;
        this.targetService = targetService;
        this.operationType = operationType;
    }

    public TokenDTO(String token, TargetService targetService, OperationType operationType) {
        this.token = token;
        this.targetService = targetService;
        this.operationType = operationType;
    }

    public TokenDTO(String token, Date expiresDate, String error) {
        this.token = token;
        this.expiresDate = expiresDate;
        this.error = error;
    }

    public TokenDTO() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiresDate() {
        return expiresDate;
    }

    public void setExpiresDate(Date expiresDate) {
        this.expiresDate = expiresDate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public TargetService getTargetService() {
        return targetService;
    }

    public void setTargetService(TargetService targetService) {
        this.targetService = targetService;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }
}
