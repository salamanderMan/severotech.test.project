package severotech.test.commonlibrary.dtos;

import java.util.Objects;

public class ReadDataSourceRequest {
    private String username;
    private String password;
    private String serverName;
    private int port;
    private String databaseName;
    private String queryTxt;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getQueryTxt() {
        return queryTxt;
    }

    public void setQueryTxt(String queryTxt) {
        this.queryTxt = queryTxt;
    }

    public ReadDataSourceRequest(String username, String password, String serverName,
                                 int port, String databaseName, String queryTxt) {
        this.username = username;
        this.password = password;
        this.serverName = serverName;
        this.port = port;
        this.databaseName = databaseName;
        this.queryTxt = queryTxt;
    }

    public ReadDataSourceRequest() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReadDataSourceRequest that = (ReadDataSourceRequest) o;
        return port == that.port && Objects.equals(username, that.username) && Objects.equals(password, that.password) && Objects.equals(serverName, that.serverName) && Objects.equals(databaseName, that.databaseName) && Objects.equals(queryTxt, that.queryTxt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, serverName, port, databaseName, queryTxt);
    }

    @Override
    public String toString() {
        return "ReadDataSourceRequest{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", serverName='" + serverName + '\'' +
                ", port=" + port +
                ", databaseName='" + databaseName + '\'' +
                ", queryTxt='" + queryTxt + '\'' +
                '}';
    }
}
